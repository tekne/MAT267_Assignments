\documentclass{article}
\usepackage[utf8]{inputenc}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage{enumitem}
\usepackage[backend=bibtex]{biblatex}
\usepackage{hyperref}

\addbibresource{references.bib}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem{claim}{Claim}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\prt}[2]{\frac{\partial #1}{\partial #2}}
\newcommand{\mprt}[3]{\frac{\partial^{#1}#2}{\partial#3}}
\newcommand{\loint}[0]{\mb{L}\int}
\newcommand{\comps}[0]{\mathbb{C}}

\DeclareMathOperator{\interior}{interior}
\DeclareMathOperator{\sgn}{sgn}
\DeclareMathOperator{\Alt}{Alt}
\DeclareMathOperator{\tr}{tr}

\title{
  Symmetry and Conservation \\
  \small{Or: An Excuse to Do Group Theory in ODE's}
}
\author{Jad Elkhaleq Ghalayini}
\date{April 2019}

\begin{document}

\maketitle

Noether's theorem is one of the most fascinating results in applied mathematics, having both elegance and a deep physical and practical significance. In brief, it demonstrates a correspondence between (continuous) \textit{symmetry} in a system (for example, invariance of the equations that describe it under changes in time or certain rotations) and \textit{conservation} of certain quantities in the system, which in physical systems manifests as properties such as conservation of energy or angular momentum. In this essay, we will given an informal exposition of Noether's theorem as it applies to mechanical systems and some consequences. But first: what's a ``mechanical system''?

Since we'll be looking at things from a physical perspective, we \textit{want} to be able to describe things which, intuitively, aught to be considered mechanical systems: a spinning top, two planets in orbit or a mass on a spring. But being a \textit{mathematical} statement, Noether's theorem tells us not about physical objects but about mathematical objects, just like how statements in analysis tell us about real numbers, not real quantities like electric charge, probability or (idealized) money. So we have to start out by thinking about how we can draw a correspondence between mathematical objects and the physical systems they describe.

A \textbf{dynamical system} is a function of the form
\begin{equation}
  \phi: \reals \times \reals^n \to \reals^n
\end{equation}
In brief, given a time \(t \in \reals\) and the values \(\mb{x} \in \reals^n\) of \(n\) real variables at time \(0\), it tells us what the values of the variables will be (or were, for \(t < 0\)) at time \(t\), namely, \(\phi(t, \mb{x})\) \autocite{odes}. We want to draw a correspondence between physical systems and dynamical systems: for that, we can start by \textit{parametrizing} the state of the system by a set of variables \(q_1,...,q_n\): these will be called \textbf{generalized coordinates} \autocite{mechanics}.

We can do this in many ways for one system, and how we choose to do this can greatly affect how easy or hard it is to solve problems about the system. Of course, for Noether's theorem to apply, we need to lay down conditions as to how exactly the system is parametrized: the first should be that the positions of everything in the system should be completely given by the generalized coordinates. So, for example, if we have two planets in orbit around each other, we want the coordinates of each planet relative to some frame of reference, giving \(6\) coordinates. In general, \(N\) particles need \(3N\) coordinates \autocite{mechanics}. The specific number of generalized coordinates necessary is called the \textbf{degrees of freedom} \autocite{mechanics}.

There is a problem with what we have so far though: while we can assume the \textit{current state} of the system is completely described by the generalized coordinates, the \textit{future state} of the system is not. Consider again the example of two planets: if the planets are at rest at time \(t = 0\), then they will fall directly towards each other, giving a dynamical system \(\phi_{fall}\). But if they are zooming past each other at very high velocity, they might barely perturb each other, giving \textit{another} dynamical system \(\phi_{zoom}\). Now if we're given the initial velocity as a parameter, when the future state is determined completely by the generalized coordinates. This is fine for something like, say, mass, which we assume to be part of the definition of the system, but we would like to think of velocity as part of the state of the system, especially since velocity tends to change over time. The simple solution, of course, is to add \(\dot{q}_1,...,\dot{q}_n\) as parameters to \(\phi\), i.e. \(2n\) variables for \(n\) degrees of freedom.

This, in fact, is going to be the first condition of our definition of a \textbf{mechanical system}: future state, velocity and position, is given completely by current state, again velocity and position \autocite{mechanics}. The second is a bit more subtle: there exists a function, \(\mc{L}: \reals^{2n + 1} \to \reals\), such that the \textbf{action}
\begin{equation}
  S(t, \mb{x}) = \int_0^t\mc{L}(t', \phi(t', \mb{x}))dt'
  \label{hamilton}
\end{equation}
is minimized uniquely by the dynamical system \(\phi\) (this is called the \textbf{principle of least action}, or \textbf{Hamilton's principle})\autocite{mechanics}. This function is called the \textbf{Lagrangian} of the system, and the behaviour of a mechanical system is completely described by it. Even better, this definition, and hence Noether's theorem, encompasses many other systems, such as simple electrical systems, which are out of the scope of this essay.

Now that we know what a mechanical system is, what does it mean for one to be ``symmetric''? Intuitively, we know a shape is symmetric if we can flip or rotate it and it still looks the same. For a mechanical system, this means the behaviour doesn't change if we flip the sign of our coordinates or rotate them. Unfortunately, these don't exactly work for Noether's theorem: it requires \textit{continuous} symmetry. \textit{Discrete} symmetries, like rotating an equilateral triangle 60 degrees or flipping it across an axis, don't work. But symmetries like those of a circle which allow us to rotate by any amount (continuously), do. While this does reduce the generality of our discussion, there is a flip side: we also get to consider continuous symmetries of \textit{time} as well, which is what we'll start with.

One especially important kind of continuous symmetry is \textit{homogeneity}: do things change if we move around in space (say, add 10 to all coordinates, moving our frame of reference)? One of the most simple, yet elegant, examples of homogeneity is the \textit{homgeneity of time}. In brief, the laws of physics don't change with time, so it makes sense to consider systems whose Lagrangian, and hence behaviour, does not depend on time, i.e.
\begin{equation}
  \forall t \in \reals, \mc{L}(t, \mb{q}, \dot{\mb{q}}) = \mc{L}(0, \mb{q}, \dot{\mb{q}}) \iff \prt{\mc{L}}{t} = 0
\end{equation}
(where \(\mb{q} = (q_1,...,q_n)\)). In this case, we have that
\begin{equation}
  \frac{d}{dt}\mc{L} = \cancel{\prt{\mc{L}}{t}} + \prt{L}{\mb{q}} \cdot \frac{d\mb{q}}{dt} + \prt{L}{\dot{\mb{q}}} \cdot \frac{d\dot{\mb{q}}}{dt}
  \label{energyp1}
\end{equation}
It turns out that the principle of least action (equation \ref{hamilton}) implies \textbf{Lagrange's equation} \autocite{mechanics}, given by
\begin{equation}
  \prt{\mc{L}}{\mb{q}} = \frac{d}{dt}\left(\prt{\mc{L}}{\dot{\mb{q}}}\right)
  \label{lagranges}
\end{equation}
Plugging this into the above (equation \ref{energyp1}), we obtain
\begin{equation}
  \frac{d}{dt}\mc{L} = \frac{d}{dt}\left(\prt{L}{\dot{\mb{q}}}\right) \cdot \dot{\mb{q}} + \prt{L}{\dot{\mb{q}}} \cdot \ddot{\mb{q}}
  = \frac{d}{dt}\left(\prt{L}{\dot{\mb{q}}} \cdot \dot{\mb{q}}\right) \iff \frac{d}{dt}\left(\prt{L}{\dot{\mb{q}}} \cdot \dot{\mb{q}} - \mc{L}\right) = 0
\end{equation}
This gives a conserved quantity (an \textbf{integral of the motion} \autocite{mechanics})
\begin{equation}
  \mc{E} = \prt{L}{\dot{\mb{q}}} \cdot \dot{\mb{q}} - \mc{L}
  \label{energydef}
\end{equation}
the \textbf{energy} of the system (as for a physical system, this quantity can be shown to correspond to kinetic energy + potential energy).

So, let's recap what we've done here. We've taken a \textit{continuous symmetry} of the system (homogeneity with respect to time) and derived a \textit{conserved quantity} (or integral of the motion), i.e. energy, with only the condition that the principle of least action is obeyed. We can derive a few more of these: if the system is \textbf{homogeneous in space}, i.e. shifting coordinates around by \(\mb{\epsilon}\) does not affect the Lagrangian, i.e.
\begin{equation}
  \mc{L}(t, \mb{r}_1, ..., \mb{r}_n, \dot{\mb{r}}_1,..., \dot{\mb{r}}_n) = \mc{L}(t, \mb{r}_1 + \mb{\epsilon}, ..., \mb{r}_n + \mb{\epsilon}, \dot{\mb{r}}_1, ... , \dot{\mb{r}}_n)
\end{equation}
where \(\mb{r}_1,...,\mb{r}_n\) are the positions of particles \(1,...,n\) in the same coordinate system, then \textbf{momentum}, given by
\begin{equation}
  \mb{P} = \sum_{i = 1}^n\prt{\mc{L}}{\dot{\mb{r}}_i}
\end{equation}
is conserved \autocite{mechanics}. Similarly, we have that if the system is \textbf{isotropic in space}, i.e. invariant of the rotation of all \(\mb{r}_i\) by the unit vector \(\mb{\Phi}\), i.e.
\begin{equation}
  \mc{L}(t, \mb{r}_1,..., \mb{r}_n, \dot{\mb{r}}_1,..., \dot{\mb{r}}_n)
  = \mc{L}(t, \mb{\Phi} \times \mb{r}_1,..., \mb{\Phi} \times \mb{r}_n, \mb{\Phi} \times \dot{\mb{r}}_1,...,\mb{\Phi} \times \dot{\mb{r}_n})
\end{equation}
then the \textbf{angular momentum}, given by
\begin{equation}
  \mb{M} = \sum_{i = 1}^n\mb{r}_i \times \mb{p}_i
  \label{angmomdef}
\end{equation}
is conserved \autocite{mechanics}.

Noether's theorem is a generalization of the derivations above, and simultaneously, as is common in abstract mathematics, can be regarded as the underlying principle of all such derivations. In brief, Noether's theorem states that for \textit{every} continuous symmetry in the Lagrangian, there is a corresponding quantity conserved. In fact, we can be even more general, and state that for every continuous symmetry that leaves the \textit{action} (the value of Equation \ref{hamilton}) invariant (even if the Lagrangian is changed) as we move along it, a corresponding conserved quantity can be derived.

Naturally, this has far reaching applications in physics: it is possible to derive all kinds of continuous symmetries and, from their existence, conclude the existence of highly non-obvious conserved quantities. More interestingly, each unique conserved quantity found can be used to eliminate a variable from the description of a system, by writing it in terms of other variables and the conserved quantity. This can be used, along with the appropriate transformations and coordinate systems, to perform extremely elegant reductions. One such reduction is that of the trajectory of two orbiting bodies (under gravity, or actually any ``central'' potential depending only on the distance between the two bodies) into a one-dimensional system \autocite{mechanics}.

To describe this reduction, we first need to define what exactly the state of two oribiting bodies is, i.e., our generalized coordinates. The obvious choice is the three-dimensional positions of each orbiting body, giving six generalized coordinates. However, this raises a question that we've so far glossed over: which frame of reference do we use? And what about units? Let's start with the second question. Thankfully, as would be hoped, units don't really matter, because the dynamics predicted by the Principle of Least Action remain the same regardless of how units describe a trajectory.

To see why, let \(p(t): \reals \to \reals^n\) be the trajectory of a particle (over time) under a set of units, and let \(p'(\beta t): \reals \to \reals^n\) be the same trajectory, but now with the generalized coordinates measured in different units (with \(\beta\) the scaling factor for time being measured by different units). Since \(\mc{L}\) is a physically-relevant number, it should have well-defined units (in fact, the same units as energy as defined in \ref{energydef}, which is often the real-life unit of energy). But this means that
\begin{equation}
  \alpha = \prod_{i = 1}^n\alpha_i^{k_i} \implies \mc{L}(\beta t, p'(\beta t)) = \beta^k\alpha\mc{L}(t, p(t))
\end{equation}
for some \(\alpha \in \reals^+, k \in \ints\),
where \(\mc{L}\) has units \(u_1^{k_1} \cdot ... \cdot u_n^{k_n}\) each scaled by \(\alpha_1,...,\alpha_n \in \reals^+\). Hence, the \textit{new} action
\begin{equation}
  \int_0^{t/\beta}\mc{L}(\beta t', p'(\beta t'))dt' = \alpha\beta^{k - 1}\int_0^t\mc{L}(t, p(t))
\end{equation}
is minimized if and only if the old one is, i.e. the trajectory is the same.

This is physically obvious, but it's nice to see that it follows from the properties of the Lagrangian given only that it itself has proper units. With that question out of the way, leaving the units as arbitrary, we are faced with a far more difficult one: the choice of frame of reference. It turns out that the Lagrangian can behave vastly differently in different frames of reference: it might even lose or gain symmetries due to the choice of frame \autocite{mechanics}. Thankfully, this can work to our advantage: it turns out that for any classical physical system we can \textit{always} find a frame of reference in which space and time are homogeneous and space is isotropic, as described above. This is called an \textbf{inertial frame}.

So what's a convenient inertial frame for our orbit problem? We can use the center of mass of the two bodies. Choosing this inertial frame, in fact, illuminate the fact that we only need to consider \textit{three} degrees of freedom. The reason for this is as follows: if we let \(\mb{r} = (r_1, r_2, r_3)\) be the vector of the displacement between the two bodies and \(m_1, m_2\) be the masses of the two bodies, then the bodies are at position
\begin{equation}
  \mb{c} + \frac{m_1}{m_1 + m_2}\mb{r}, \mb{c} + \frac{m_2}{m_1 + m_2}\mb{r}
\end{equation}
respectively, where \(\mb{c}\) is the position of the center of mass. Since in the center of mass frame we have \(\mb{c} = 0\), the state of the system is completely described by \(\mb{r}\) (since everything else is a constant).

Now that we have the state of the system, how can we get a Lagrangian corresponding to it's behaviour? We can derive from Newton's laws that an appropriate Lagrangian is given by
\begin{equation}
  \mc{L} = T - U
\end{equation}
where \(T, U\) are kinetic and potential energy respectively. So, assuming the bodies have potential energy \(U(\mb{r})\) dependent only on \(|\mb{r}|\), for example gravity given by
\begin{equation}
  U_g(\mb{r}) = -\frac{Gm_1m_2}{r}
\end{equation}
where \(r = |\mb{r}|\), and using the definition of kinetic energy
\begin{equation}
  E = \frac{1}{2}mv^2 = \frac{1}{2}m\dot{\mb{r}}^2
\end{equation}
we get
\begin{equation}
  \mc{L}(t, \mb{r}, \dot{\mb{r}}) = \frac{1}{2}\mu\dot{\mb{r}}^2 - U(\mb{r})
\end{equation}
where \(\mu\) is the \textbf{reduced mass} \(\frac{m_1 + m_2}{m_1m_2}\) \autocite{mechanics}, obtained through algebra.
Note that in general we can reduce a system of \(n + 1\) particles to a system of \(n\) ``particles'' by using the center of mass frame.

Now comes the fun part: we will introduce symmetries and watch the variables drop out via Noether's theorem, or rather the specific instantiations of it described above. Currently, \(\mc{L}\) is not isotropic or homgeneous with \(\mb{r}\), as the directions of both \(\mb{r}\) and \(\dot{\mb{r}}\) may factor into account, so the only thing we know is that energy \(\mc{E}\) as defined in Equation \ref{energydef} is conserved, since \(\mc{L}\) does not depend directly on \(t\). Assume, however, that \(U(\mb{r})\) depends only on \(r\) (like gravity), i.e. can be written \(U(r)\).
Then, since \(\mc{L}\) no longer depends on the direction of \(\mb{r}\), Noether's theorem tells us that angular momentum \(\mb{M}\), as defined in equation \ref{angmomdef}, is conserved. But it follows from a physical argument that
\begin{equation}
  \mb{M} = \mb{r} \times \mb{p}
\end{equation}
where \(\mb{p}\) is momentum, and hence that \(\mb{M}\) is perpendicular to \(\mb{r}\). Since it is conserved, it follows that \(\mb{r}\) remains in the plane which \(\mb{M}\) is normal to, and can be described in polar coordinates \((r, \phi)\) within that plane \autocite{mechanics}. Physically, this is obvious (imagine planets orbiting in your head), but it's nice to see the mathematics confirming our intuition.

So \textit{now} we can rewrite our Lagrangian as
\begin{equation}
  \mc{L}(r, \dot{r}, \dot{\phi}) = \frac{1}{2}m(\dot{r}^2 + r^2\dot{\phi}^2) - U(r)
\end{equation}
Note that \(\mc{L}\) does not depend directly on \(\phi\), only on it's first derivative. Then Lagrange's equation (equation \ref{lagranges}) tells us that
\begin{equation}
  \frac{d}{dt}\left(\prt{\mc{L}}{\dot{\phi}}\right) = p_\phi = \frac{d}{dt}\left(\mu r^2\dot{\phi}\right) = \prt{\mc{L}}{\phi} = 0
\end{equation}
i.e. that the \textbf{generalized momentum} \(p_\phi\), as defined above, is an integral of the motion. Note physically that this is equal to the magnitude of angular momentum along the \(z\)-axis, i.e. \(M_z = M\).

We pause for a moment to note that this is actually another, equivalent, statement of Noether's theorem: terms dependent only on the first derivative of a generalized coordinate correspond to conserved quantities and to symmetries. The reason for this is that such terms can be shown to leave the action invariant, leading to a continuous symmetry and hence by Noether's theorem to a conserved quantity. Going backwards, however, appropriate coordinate transformations and conserved quantities can be used to generate such terms, hence the correspondence.

Now all we have to do is plug this in to our Lagrangian to obtain
\begin{equation}
  \mc{L}(r, \dot{r}) = \frac{1}{2}\mu\dot{r}^2 + \frac{M^2}{2mr^2} + U(r)
\end{equation}
We're down to two variables. But remember energy is conserved, so we can relate them by definition (equation \ref{energydef}) as
\begin{equation}
  \mc{E} = \mu(\dot{r}^2 + r^2\dot{\phi}^2) - \left(\frac{1}{2}\mu\dot{r}^2 + \frac{M^2}{2mr^2} - U(r)\right) = \frac{1}{2}\mu\dot{r}^2 + \frac{M^2}{2mr^2} + U(r)
\end{equation}
which gives the simple first-order ODE
\begin{equation}
  \dot{r} = \frac{2}{\mu}\sqrt{\mc{E} - \frac{M^2}{2mr^2} - U(r)}
\end{equation}
which can be solved through traditional methods.

This problem is probably the simplest one which can be attempted using the methods which Noether's theorem opens up to us, and yet at least in my opinion the elegance of the related formalism really shines through. Symmetry allows us to, through algebraic manipulations, ``peel'' the complexity off a system in an almost Platonic manner. More beautifully still, however, these same symmetries not only allow one to derive all the laws of Newtonian mechanics in a new and illuminating manner, but also provide deep insight into quantum mechanics, which makes extensive use of the concepts first introduced in Hamiltonian mechanics. Overall, then, I believe that Noether's theorem is a counter-example to the common stereotype that mathematics has to be useless to be beautiful. Disregarding the fact that even number theory finds applications in cryptography, Noether's theorem is not only a stunningly beautiful result, but as physically real as mathematics gets.

\printbibliography

Note: this essay was heavily based on material covered in the course PHY354, taken in Winter 2018 and taught by Professor David Curtin. My notes for the course can be found at \url{https://gitlab.com/tekne/PHY354_Notes}

\end{document}
