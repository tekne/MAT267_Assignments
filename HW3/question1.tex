\documentclass{article}
\usepackage[utf8]{inputenc}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage{enumitem}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem{claim}{Claim}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\prt}[2]{\frac{\partial #1}{\partial #2}}
\newcommand{\mprt}[3]{\frac{\partial^{#1}#2}{\partial#3}}
\newcommand{\loint}[0]{\mb{L}\int}
\newcommand{\comps}[0]{\mathbb{C}}

\DeclareMathOperator{\interior}{interior}
\DeclareMathOperator{\sgn}{sgn}
\DeclareMathOperator{\Alt}{Alt}
\DeclareMathOperator{\tr}{tr}

\title{MAT267 Homework 3, Question 1}
\author{Jad Elkhaleq Ghalayini}
\date{March 2019}

\begin{document}

\maketitle

Note: this proof is based heavily off the one in Hirsch, Smale and Devaney.

\begin{theorem}

Let \(\mc{O} \subseteq \reals \times \reals^n\) be open and \(F: \mc{O} \to \reals^n\) be a function such that \(F(t, X)\) is \(C^1\) in \(X\) and \(C^0\) in \(t\). Then if \((t_0, X_0) \in \mc{O}\),
there exists a closed interval \(J\) (and hence an open interval within it) containing \(t_0\) and a unique solution \(X: J \to \reals^n\) of the ODE
\begin{equation}
  X(t)' = F(t, X(t))
\end{equation}
satisfying \(X(0) = X_0\).

\end{theorem}
\begin{proof}

  Assume without loss of generality that \(t_0 = 0\). Furthermore, let \(\mc{O}_\rho \subset \reals^n\) denote the closed ball centered at \(X_0\) of radius \(\rho \in \reals^+\), where \(\rho\) is arbitrary such that we can find \(b \in \reals^+\)
  \[(-b, b) \times \mc{O}_\rho \subset \mc{O}\]
  Since \(F(t)\) is \(C^1\) on \(\mc{O}_\rho\), we have that it is locally Lipschitz with constant \(K\). Furthermore, since \(F\) is \(C^0\) on \((-b, b) \times \mc{O}_\rho\), it is bounded and hence has maximum absolute value \(M\). Take now \(J = [-a, a]\) where
  \[a < \min\{p/M, 1/K, b\}\]
  We begin by defining an infinite collection of functions \(U_n : J \to \reals^n\) as follows: define
  \[U_0(t) = X_0\]
  Then, for each \(n \in \nats\), define
  \[U_{n + 1}(t) = X_0 + \int_{0}^tF(s, U_k(s))ds\]
  Assume now that, for some \(k \in  \nats\), \(|U_k(t) - X_0| < \rho\) (note this is obviously true for \(U_0\), as \(|U_0(t) - X_0| = 0\)). We have that
  \[|U_{k + 1} - X_0| \leq \int_0^t|F(s, U_k(s))|ds \leq \int_0^tMds \leq Ma < \rho\]
  It follows by induction that
  \[\forall n \in \nats, |U_n - X_0| < \rho\]
  Now, we want to find a constant \(L \leq 0\) such that
  \[\forall k \in \nats, |U_{k + 1}(t) - U_k| \leq (aK)^kL\]
  Choose
  \[L = \max_{J}|U_1 - U_0| = \max_{[-a, a]}|U_1 - X_0| \leq aM\]
  We have that
  \[|U_2(t) - U_1(t)| = \left|\int_0^tF(s, U_1(s)) - F(s, X_0)ds\right| \leq \int_0^t|F(s, U_1(s)) - F(s, X_0)|\]
  Since \(F(t, X)\) is Lipschitz in \(X\), it follows that the above is less than or equal to
  \[\int_0^tK|U_1(s) - X_0| \leq aKL\]
  Now, assume that for some \(k \geq 2\) we have that
  \[|t| \leq a \implies |U_k(t) - U_{k - 1}(t)| \leq (aK)^{k - 1}L\]
  Then
  \[|U_{k + 1}(t) - U_k(t)| \leq \int_0^t|F(s, U_k(s)) - F(s, U_{k - 1}(s))|ds \leq K\int_0^t|U_k(s) - U_{k - 1}(s)|ds \leq (aK)(aK)^{k - 1}L = (aK)^kL\]
  Let \(\alpha = aK < \frac{1}{K}K = 1\). Then, giving \(\epsilon > 0\), we can find \(N\) such that
  \[\forall r > s > n, |U_r(t) - U_s(t)| \leq \sum_{k = N}^\infty|U_{k + 1}(t) - U_k(t)| \leq  \sum_{k = N}^\infty\alpha^kL = L\sum_{k = N}^\infty\alpha^k \leq \epsilon\]
  by the properties of the geometric series with ratio \(\alpha\). By the lemma from analysis, we have that \(U_1,U_2,...\) converges uniformly to a continuous function \(X: J \to \reals^n\). We have that
  \[U_{k + 1}(t) = X_0 + \int_0^tF(s, U_k(s))ds \implies X(t)
  = X_0 + \lim_{k \to \infty}\int_0^tF(s, U_k(s))ds
  = X_0 + \int_0^t\lim_{k \to \infty}F(U_k(s))ds\]
  \[= X_0 + \int_0^tF(s, X(s))ds \implies X(0) = X_0, X'(t) = \frac{d}{dt}\int_0^tF(s, X(s))ds = F(t, X(t))\]
  i.e. that a solution \(X\) exists.

  For uniqueness, suppose that \(X, Y: J \to \mc{O}\) are two solutions to the ODE above satisfying \(X(0) = Y(0) = X_0\), with \(J\) the closed interval from above. Let
  \[Q = \max_{J}|X - Y|\]
  This maximum is attained at some point \(t_1 \in J\). We have
  \[Q = |X(t_1) - Y(t_1)| = \left|\int_0^{t_1}(X'(s) - Y'(s))ds\right| \leq \int_0^{t_1}|F(s,X(s)) - F(s,Y(s))|ds \leq \int_0^{t_1}|X(s) - Y(s)|ds \leq aKQ\]
  But
  \[aK < 1 \implies Q \leq 0 \implies Q = 0 \implies X(t) = Y(t)\]
  i.e. \(X\) is unique.

\end{proof}

\end{document}
