 \documentclass{article}
\usepackage[utf8]{inputenc}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage{enumitem}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem{claim}{Claim}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\prt}[2]{\frac{\partial #1}{\partial #2}}
\newcommand{\mprt}[3]{\frac{\partial^{#1}#2}{\partial#3}}
\newcommand{\loint}[0]{\mb{L}\int}
\newcommand{\comps}[0]{\mathbb{C}}

\DeclareMathOperator{\interior}{interior}
\DeclareMathOperator{\sgn}{sgn}
\DeclareMathOperator{\Alt}{Alt}
\DeclareMathOperator{\tr}{tr}

\title{MAT267 Homework 4, Question 2}
\author{Jad Elkhaleq Ghalayini}
\date{March 2019}

\begin{document}

\maketitle

\begin{enumerate}[label=(\alph*)]

  \item \begin{itemize}

    \item We have, due to the location of the critical points,
    \[DU_1(x) = ax(x + 1)(x - 2) = a(x^3 - x^2 - 2x) \implies U_1(x) = a(x^4/4 - x^3/3 - x^2) + c\]
    \[U_1(0) = 0 \implies c = 0, U_1(2) = a(4 - 8/3 - 4) = 8a/3 = -8/3 \implies a = 1\]
    giving
    \[U_1(x) = x^4/4 - x^3/3 - x^2\]
    Checking our work, we have
    \[U_1(-1) = 1/4 + 1/3 - 1 = 7/12 - 12/12 = -5/12\]
    as desired.

    \item We have, due to the location of the critical points,
    \[DU_2(x) = a(x + 1)(x - 1) = a(x^2 - 1) \implies U_2(x) = a(x^3/3 - x) + c\]
    \[U_2(1) = a(1/3 - 1) + c = -2a/3 + c = 2/3, U_2(-1) = a(2/3) + c = -\frac{2}{3} \implies c = 0, a = -1\]
    Hence, we obtain
    \[U_2(x) = x - \frac{x^3}{3}\]

    \item We have, due to the location of the critical points,
    \[DU_3(x) = ax(x - 1)(x + 1) = a(x^3 - x) \implies U_3(x) = a(x^4/4 - x^2/2) + c\]
    \[U_3(0) = 0 \implies c = 0 \implies U_3(1) = a(1/4 - 1/2) = -a/4 = 1/4 \iff a = -1\]
    giving
    \[U_3(x) = x^2/2 - x^4/4\]
    Checking, we have
    \[U_3(-1) = -(1/4 - 1/2) = 1/4\]
    as desired.

  \end{itemize}

  \item Using our results from Question 1, we have first order systems
  \begin{itemize}

    \item \(U_1\):
    \[X' = \begin{pmatrix} x_2 \\ -DU_1(x_1)/2 \end{pmatrix}
      = \begin{pmatrix}
        x_2 \\
        x + x^2/2 - x^3/2
      \end{pmatrix}
    \]
    For the plot, see Figure \ref{fig:plot1}

    \item \(U_2\):
    \[X' = \begin{pmatrix} x_2 \\ -DU_2(x_1)/2 \end{pmatrix}
      = \begin{pmatrix}
        x_2 \\
        x^2/2 - 1/2
      \end{pmatrix}
    \]
    For the plot, see Figure \ref{fig:plot2}

    \item \(U_3\):
    \[X' = \begin{pmatrix} x_2 \\ -DU_3(x_1)/2 \end{pmatrix}
      = \begin{pmatrix}
        x_2 \\
        x^3/2 - x/2
      \end{pmatrix}
    \]
    For the plot, see Figure \ref{fig:plot3}

  \end{itemize}

  \item \begin{itemize}

    \item We have critical values \(U_1(0) = 0\), giving
    \[\mc{L} = x_2^2 + U_1(x_1) = U_1(0) = 0 \iff x_2^2 = -U_1(x_1) \iff x_2 = \sqrt{-U_1(x_1)}\]
    yielding the critical level set
    \[L = \left\{(x_1, \sqrt{-U_1(x_1)}) : x_1 \in \reals\right\} \cap \reals^2\]
    Note that to the right of this critical level set, which includes the origin, solutions ``orbit'' around the local minima at \(x_1 = -1\), whereas to the right solutions ``orbit'' the local minima at \(x_1 = 1\), with the critical set dividing the plot into two halves. This can clearly be seen in the structure of the graph.

    \item We have critical values \(U_2(1) = 2/3\), giving
    \[\mc{L} = x_2^2 + U_2(x_1) = U_2(1) = 2/3 \iff x_2^2 = 2/3 - U_2(x_1) \iff x_2 = \sqrt{2/3 - U_2(x_1)}\]
    yielding the critical level set
    \[L = \left\{(x_1, \sqrt{2/3 - U_2(x_1)}) : x_1 \in \reals\right\} \cap \reals^2\]
    Notice that to the right of this critical level set, which includes the point \((1, 2/3)\), solutions orbit the local minima at \(x_1 = -1\), but to the left they fly off to infinity.

    \item We have critical values \(U_3(-1) = 1/4, U_3(1) = 1/4\), giving the single equation (since the values of \(U\) at the critical points are the same)
    \[\mc{L} = x_2^2 + U_3(x_1) = U_3(1) = 1/4 \iff x_2^2 = 1/4 - U_3(x_1) \iff x_2 = \sqrt{1/4 - U_3(x_1)}\]
    yielding the critical level set
    \[L = \left\{(x_1, \sqrt{2/3 - U_3(x_1)}) : x_1 \in \reals\right\} \cap \reals^2\]
    We can see enclosed by this critical level set, with sides at \(x_1 = 1, x_1 = -1\), a region ``orbiting'' around the minimum at \(x_1 = 0\), while above the critical set and to the right solutions go to infinity, and below the critical set and to the left solutions go to negative infinity.


  \end{itemize}

  \item We have from Question 1 that
  \[\frac{d}{dt}\mc{L} = 2x_2\dot{x_2} + U'(x_1)\dot{x_1}\]
  Consider the system
  \[X' = F(X) = \begin{pmatrix}
    x_2 - \epsilon \\
    -U'(x_1)/2 - \epsilon
  \end{pmatrix}\]
  for some \(\epsilon \in \reals^+\). Plugging back into our formula, we have
  \[\frac{d}{dt}\mc{L} = 2x_2(\cancel{-U'(x_1)/2}) + \cancel{U'(x_1)x_2} - \epsilon = -2\epsilon < 0\]
  Let's try a (relatively) small \(\epsilon = 0.25\), for each of \(U_1, U_2, U_3\). Plotting, we obtain Figures \ref{fig:q2u1p}, \ref{fig:q2u2p}, \ref{fig:q3u3p} respectively.

\end{enumerate}

\begin{figure}
  \begin{center}
    \includegraphics[width=0.6\linewidth]{q2u1}
  \end{center}
  \caption{Plot of first order ODE obtained from potential \(U_1\)}
  \label{fig:plot1}
\end{figure}

\begin{figure}
  \begin{center}
    \includegraphics[width=0.6\linewidth]{q2u2}
  \end{center}
  \caption{Plot of first order ODE obtained from potential \(U_2\)}
  \label{fig:plot2}
\end{figure}

\begin{figure}
  \begin{center}
    \includegraphics[width=0.6\linewidth]{q2u3}
  \end{center}
  \caption{Plot of first order ODE obtained from potential \(U_3\)}
  \label{fig:plot3}
\end{figure}

\begin{figure}
  \begin{center}
    \includegraphics[width=0.6\linewidth]{q2u1p}
  \end{center}
  \caption{Plot of first order ODE obtained from perturbing the first order ODE with potential \(U_1\) by subtracting \(2\epsilon = 0.5\) from the second component of the derivative}
  \label{fig:q2u1p}
\end{figure}

\begin{figure}
  \begin{center}
    \includegraphics[width=0.6\linewidth]{q2u1p}
  \end{center}
  \caption{Plot of first order ODE obtained from perturbing the first order ODE with potential \(U_1\) by subtracting \(2\epsilon = 0.5\) from the second component of the derivative}
  \label{fig:q2u2p}
\end{figure}

\begin{figure}
  \begin{center}
    \includegraphics[width=0.6\linewidth]{q2u1p}
  \end{center}
  \caption{Plot of first order ODE obtained from perturbing the first order ODE with potential \(U_1\) by subtracting \(2\epsilon = 0.5\) from the second component of the derivative}
  \label{fig:q3u3p}
\end{figure}


\end{document}
